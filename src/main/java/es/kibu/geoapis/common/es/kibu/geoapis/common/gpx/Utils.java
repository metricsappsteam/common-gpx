/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.common.es.kibu.geoapis.common.gpx;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.joda.time.MutableDateTime;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Created by lrodriguez2002cu on 08/03/2016.
 */
public class Utils {

    public static final int INVALID_INT = -2147483648;

    public static DateTime fromTime(XMLGregorianCalendar time) {
        try {
            int millisecond = time.getMillisecond();
            MutableDateTime dt = new
                    MutableDateTime(time.getYear(), time.getMonth(), time.getDay(),
                    time.getHour(), time.getMinute(), time.getSecond(), millisecond == INVALID_INT? 0: millisecond,
                    DateTimeZone.forOffsetHours(time.getTimezone()));
            return dt.toDateTime();
        }
        catch (NullPointerException ex) {
            ex.printStackTrace();
            throw  ex;
        }
    }

    public static DateTime updateMin(DateTime minDateTime, DateTime dt) {
        if (dt == null) return minDateTime;
        if (minDateTime == null) return dt;
        if (dt.isBefore(minDateTime)) return dt;
        return minDateTime;
    }


    public static DateTime updateMax(DateTime maxDateTime, DateTime dt) {
        if (dt == null) return maxDateTime;
        if (maxDateTime == null) return dt;
        if (dt.isAfter(maxDateTime)) return dt;
        return maxDateTime;
    }

    public static double updateMax(Double max, Double newDistance){
        if (max == null) return newDistance;
        if (max < newDistance) return newDistance;
        return max;
    }

    public static double updateMin(Double min, Double newDistance){
        if (min == null) return newDistance;
        if (min > newDistance) return newDistance;
        return min;
    }

    public static boolean isContained(DateTime begin, DateTime end, DateTime dateTime, boolean allowOpenInterval){
        boolean compliesTimeRestriction = false;

        Interval interval = (begin != null && end != null) ? new Interval(begin, end) : null;
        if (interval != null) {
            compliesTimeRestriction =//interval.contains(dateTime)
                    (interval.getStart().isBefore(dateTime) || (interval.getStart().isEqual(dateTime)))
                            && (interval.getEnd().isAfter(dateTime) || interval.getEnd().isEqual(dateTime));
        }
         else {
            if (allowOpenInterval) {
                //if only the end is specified
                if (begin == null && end != null) {
                    compliesTimeRestriction = end.isAfter(dateTime);
                } else {
                    //only the begining is specified
                    if (begin != null && end == null) {
                        compliesTimeRestriction = dateTime.isAfter(begin);
                    }
                    else {
                        //both endings opened
                        return true;
                    }
                }


            }

        }
        return compliesTimeRestriction;
    }
}
