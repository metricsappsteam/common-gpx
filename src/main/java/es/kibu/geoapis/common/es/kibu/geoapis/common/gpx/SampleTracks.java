/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.common.es.kibu.geoapis.common.gpx;

import javax.xml.bind.JAXBException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lrodr_000 on 07/03/2016.
 */
public class SampleTracks {


    public static final String SAMPLE1 = "la-cabra-por-almunecar-8-11-2012-granada.xml";
    public static final String SAMPLE2 = "middle-teton-climb-mid-august-in-minimal-snow.xml";
    public static final String SAMPLE3 = "pico-de-abantos-1-763-m-desde-san-lorenzo-del-escorial-monte.xml";
    public static final String SAMPLE4 = "iznalloz-pto-zegri-colomera-18-09-2014.xml";


    public static List<String> samples = new ArrayList<String>();

    static {
        samples.add(SAMPLE1);
        samples.add(SAMPLE2);
        samples.add(SAMPLE3);
        samples.add(SAMPLE4);
    }

    private static InputStream getResourceStream(String path){
        InputStream in = Gpx.class.getResourceAsStream("/"+path);
        return in;
    }

    public static Sample getGPX(String resource) throws JAXBException {
        InputStream in = getResourceStream(resource);
        Gpx gpx = (Gpx) new GPXApi().readGpx(in);
        return new Sample(gpx, resource);
    }

    public static Sample getGPX1() throws JAXBException {
        return getGPX(SAMPLE1);
    }

    public static Sample getGPX2() throws JAXBException {
        return getGPX(SAMPLE2);
    }

    public static Sample getGPX3() throws JAXBException {
        return getGPX(SAMPLE3);
    }

    public static Sample getGPX4() throws JAXBException {
        return getGPX(SAMPLE4);
    }

    public static class Sample {

        public Gpx getGpx() {
            return gpx;
        }

        public void setGpx(Gpx gpx) {
            this.gpx = gpx;
        }

        public String getSampleName() {
            return sampleName;
        }

        public void setSampleName(String sampleName) {
            this.sampleName = sampleName;
        }

        public Sample(Gpx gpx, String sampleName) {
            this.gpx = gpx;
            this.sampleName = sampleName;
        }

        Gpx gpx;
        String sampleName;
    }

    public static List<Sample> getAllTracks() throws JAXBException {
        List<Sample> gpxs = new ArrayList<Sample>();
        for (String sampleName : samples) {
            Sample sample= getGPX(sampleName);
            gpxs.add(sample);
        }
        return gpxs;
    }

}
