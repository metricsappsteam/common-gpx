/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.common.es.kibu.geoapis.common.gpx;

import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;

/**
 * Created by lrodr_000 on 07/03/2016.
 */
public class GPXApiTest extends TestCase {


    String[] resources = new String[]{
            "la-cabra-por-almunecar-8-11-2012-granada.xml",
            //"iznalloz-pto-zegri-colomera-18-09-2014.xml",
            //"ruta-motera-por-coll-de-rates-callosa-y-castell-de-guadalest.xml", NOTE: this route has no waypoints
            "middle-teton-climb-mid-august-in-minimal-snow.xml",
            "pico-de-abantos-1-763-m-desde-san-lorenzo-del-escorial-monte.xml"
    };

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    private InputStream getResourceStream(String path) {
        InputStream in = getClass().getResourceAsStream("/" + path);
        return in;
    }

    public void testLoadGPX() throws Exception {
        for (String resource : resources) {
            System.out.println("Testing:  " + resource);

            InputStream in = getResourceStream(resource);
            try {

                String str =  IOUtils.toString(in, "UTF-8");

                //System.out.println(str);

                InputStream inputStream = IOUtils.toInputStream(str);

                Gpx gpx = (Gpx) new GPXApi().readGpx(inputStream);
                System.out.println(gpx.toString());
                assertTrue(gpx.getWayPointsCount() > 0);
            }
            finally {
                 in.close();
            }

        }
    }


    public void testLoadGPXPicoDeAvantos() throws Exception {
        //for (String resource : resources) {
            String resource = "pico-de-abantos-1-763-m-desde-san-lorenzo-del-escorial-monte.xml";
            System.out.println("Testing:  " + resource);

            InputStream in = getResourceStream(resource);
            try {

                //String str =  IOUtils.toString(in, "UTF-8");

                //System.out.println(str);

                //InputStream inputStream = IOUtils.toInputStream(str);

                Gpx gpx = (Gpx) new GPXApi().readGpx(in);
                System.out.println(gpx.toString());
                assertTrue(gpx.getWayPointsCount() > 0);
            }
            finally {
                 in.close();
            }

        //}
    }

    //"pico-de-abantos-1-763-m-desde-san-lorenzo-del-escorial-monte.xml"
}