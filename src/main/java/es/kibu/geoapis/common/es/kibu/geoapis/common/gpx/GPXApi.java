/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.common.es.kibu.geoapis.common.gpx;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;

/**
 * Created by lrodriguez2002cu on 07/03/2016.
 */
public class GPXApi {

    public GpxType readGpx(File file) throws JAXBException {
        return JAXB.unmarshal(file, Gpx.class);
    }

    public GpxType readGpx(InputStream in) throws JAXBException {
        return JAXB.unmarshal(in, Gpx.class);
    }

    public GpxType readGpx(String in) throws JAXBException {
        return JAXB.unmarshal(in, Gpx.class);
    }

    public GpxType readGpx(Reader in) throws JAXBException {
        return JAXB.unmarshal(in, Gpx.class);
    }

    public void writeGpx(GpxType gpxFile, File file) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(GpxType.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        ObjectFactory obf = new ObjectFactory();
        m.marshal(obf.createGpx(gpxFile), file);
    }

    public void writeGpx(GpxType gpxFile, OutputStream outputStream) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(GpxType.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        ObjectFactory obf = new ObjectFactory();
        m.marshal(obf.createGpx(gpxFile), outputStream);
    }


    public String writeGpxToString(GpxType gpxFile) throws JAXBException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        JAXBContext jc = JAXBContext.newInstance(GpxType.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        ObjectFactory obf = new ObjectFactory();
        m.marshal(obf.createGpx(gpxFile), stream);

        return stream.toString();
    }

}
