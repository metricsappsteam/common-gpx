/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.common.es.kibu.geoapis.common.gpx;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lrodr_000 on 07/03/2016.
 */
public class Gpx extends GpxType {

    public int getRouteCount() {
        return this.getRte().size();
    }

    public int getTracksCount() {
        return this.getTrk().size();
    }

    public int getWayPointsCount() {
        return this.getWpt().size();
    }

    @Override
    public String toString() {
        return String.format("Gpx{ routes %s, waypoints: %s, tracks: %s}", getRouteCount(),
                getWayPointsCount(), getTracksCount());
    }

    public TrackInfo getTrackInfo(int index){
        TrackInfo info = new TrackInfo(this.getTrk().get(index));
        return info;
    }

    public TrackInfo getTrackInfo(TrkType track){
        if (this.getTrk().contains(track)) {
            TrackInfo info = new TrackInfo(track);
            return info;
        }
        return null;
    }

    public static class SegmentInfo {

        TrksegType segment;
        int segmentWayPoints;

        public DateTime getMinTime() {
            return minTime;
        }

        public void setMinTime(DateTime minTime) {
            this.minTime = minTime;
        }

        public DateTime getMaxTime() {
            return maxTime;
        }

        public void setMaxTime(DateTime maxTime) {
            this.maxTime = maxTime;
        }

        DateTime minTime = null;
        DateTime maxTime = null;

        Interval getInterval() {
            return new Interval(minTime, maxTime);
        }

        public SegmentInfo(TrksegType segment) {
            this.segment = segment;
        }
    }


    public class TrackInfo {

        private final TrkType track;

        DateTime minTime = null;
        DateTime maxTime = null;

        Map<TrksegType, SegmentInfo> segmentsInfo = new HashMap<TrksegType, SegmentInfo>();

        private DateTime updateMin(DateTime minDateTime, DateTime dt) {
            if (minDateTime == null) return dt;
            if (dt.isBefore(minDateTime)) return dt;
            return minDateTime;
        }


        private DateTime updateMax(DateTime maxDateTime, DateTime dt) {
            if (maxDateTime == null) return dt;
            if (dt.isAfter(maxDateTime)) return dt;
            return maxDateTime;
        }

        public TrackInfo(TrkType track) {
            this.track = track;
            prepareInfo();
        }


        public Interval getInterval() {

            return new Interval(minTime, maxTime);
        }

        int totalWayPoints = 0;
        int totalSegments = 0;

        public int getTotalSegments() {
            return totalSegments;
        }

        public int getTotalWayPoints() {
            return totalWayPoints;
        }

        private void prepareInfo() {
            if (minTime == null && maxTime == null) {

                for (TrksegType trksegType : track.getTrkseg()) {
                    DateTime segmentMin = null;
                    DateTime segmentMax = null;


                    for (WptType wptType : trksegType.getTrkpt()) {

                        DateTime time = Utils.fromTime(wptType.getTime());
                        minTime = updateMin(minTime, time);
                        maxTime = updateMax(maxTime, time);

                        segmentMin = updateMin(segmentMin, time);
                        segmentMax = updateMin(segmentMax, time);
                        totalWayPoints++;
                    }

                    //fill the segment info
                    SegmentInfo segmentInfo = new SegmentInfo(trksegType);
                    segmentInfo.setMinTime(segmentMin);
                    segmentInfo.setMaxTime(segmentMax);

                    segmentsInfo.put(trksegType, segmentInfo);
                    totalSegments++;
                }
            }

        }

        @Override
        public String toString() {
            return "TrackInfo{" +
                    "minTime=" + minTime +
                    ", maxTime=" + maxTime +
                    ", totalWayPoints=" + totalWayPoints +
                    ", totalSegments=" + totalSegments +
                    '}';
        }
    }
}

